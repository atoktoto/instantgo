package controllers

import play.api.mvc._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import javax.ws.rs.{PathParam, QueryParam}
import models.{GameRoom, AppDB, Bar, Game}
import org.squeryl.PrimitiveTypeMode.inTransaction
import org.squeryl.PrimitiveTypeMode._
import models.Game._

object Api extends Controller {

  implicit val gameWrites = new Writes[Game] {
    def writes(c: Game): JsValue = {
      Json.obj(
        "id" -> c.id,
        "play_url_white" -> c.play_url_white,
        "play_url_black" -> c.play_url_black,
        "api_url" -> c.api_url,
        "board" -> c.board,
        "state" -> c.state,
        "captured_white" -> c.captured_white,
        "captured_black" -> c.captured_black,
        "result_white" -> c.result_white,
        "result_black" -> c.result_black,
        "white_player_key" -> c.white_player_key,
        "black_player_key" -> c.black_player_key,
        "spectator_player_key" -> c.spectator_player_key
      )
    }
  }

  implicit val barWrites = new Writes[Bar] {
    def writes(c: Bar): JsValue = {
      Json.obj(
        "id" -> c.id,
        "name" -> c.name
      )
    }
  }

  def getOptions(path: String) = Action { implicit request => Ok("") }

  implicit val rds = (
    (__ \ 'name).read[String] and
      (__ \ 'age).read[Long]
    ) tupled

  def sayHello()
    = Action { request =>
    request.body.asJson.map { json =>
      json.validate[(String, Long)].map{
        case (name, age) => {
          val resp = Json.obj("response" -> ("Hello " + name + ", you're "+age))
          Ok(resp)
        }
      }.recoverTotal{
        e => BadRequest("Detected error:"+ JsError.toFlatJson(e))
      }
    }.getOrElse {
      BadRequest("Expecting Json data")
    }
  }

  def createGame() = Action {
    val game = Game.create()
    Ok(Json.toJson(game))
  }

  def getGame(key: String) = Action {
    val game = Game.getByPlayerKey(key)
    Ok(Json.toJson(game))
  }

  def playMove(coord: String, key: String) = Action {
    var game = Game.getByPlayerKey(key)

    val moveResult = game.makeMove(coord, key)

    moveResult match {
      case r:MoveResultSuccess => {
        val newGame = Game.getById(game.id)
        val json = Json.toJson(newGame)
        GameRoom.sendGameActionNotification(game.id, json)
        Ok(json).as(JSON)
      }
      case r:MoveResultFailure => BadRequest(r.cause).as(JSON)
    }
  }

  def getGameState() = Action {
    Ok("")
  }

  def getBars = Action {

    inTransaction {
      AppDB.barTable.insert(new Bar("another bar"))
    }

    val json = inTransaction {
      val bars = from(AppDB.barTable)(barTable =>
        select(barTable)
      )
      Json.toJson(bars)
    }

    Ok(json).as(JSON)
  }

}