package controllers

import play.api._
import play.api.mvc._
import play.api.db.DB
import anorm._
import play.api.Play.current
import play.api.libs.json._
import play.api.libs.functional.syntax._
import javax.ws.rs.{PathParam, QueryParam}

object ApiDocu extends Controller {

  val basePath = Play.configuration.getString("application.baseUrl").get;
  val apiVersion = "beta";

  val helloApi = Json.obj(
    "path" -> "/hello",
    "description" -> "says hello",
    "operations" -> Json.arr(
      Json.obj(
        "httpMethod" -> "POST",
        "nickname" -> "getHello",
        "parameters" -> Json.arr(
          Json.obj(
            "paramType" -> "body",
            "description"-> "Your name",
            "dataType" -> "HelloObject",
            "required" -> "true"
          )
        )
      )
    )
  )

  val createGame = Json.obj(
    "path" -> "/game",
    "description" -> "creates new game",
    "operations" -> Json.arr(
      Json.obj(
        "httpMethod" -> "POST",
        "nickname" -> "createGame",
        "parameters" -> Json.arr()
      )
    )
  )

  val getGame = Json.obj(
    "path" -> "/game/{gameId}",
    "description" -> "Gets game by ID",
    "operations" -> Json.arr(
      Json.obj(
        "httpMethod" -> "GET",
        "nickname" -> "getGame",
        "parameters" -> Json.arr(
          Json.obj(
            "paramType" -> "path",
            "name" -> "gameId",
            "dataType" -> "string",
            "description"-> "Game ID",
            "required" -> "true"
          )
        )
      )
    )
  )

  def index(api:String) = Action { req =>
    if(api == "api") {

      val js = Json.obj(
        "apiVersion" -> apiVersion,
        "swaggerVersion" -> "1.1",
        "basePath" -> (basePath + "api"),
        "resourcePath"-> "/api",
        "apis" -> Json.arr(
          helloApi, createGame, getGame
        ),
        "models" -> Json.obj(
          "HelloObject" -> Json.obj(
            "id" -> "HelloObject",
            "properties" -> Json.obj(
              "name" -> Json.obj("type" -> "string"),
              "age" -> Json.obj("type" -> "integer")
            )
          )
        )
      )
      Ok(js)

    } else {

      val js = Json.obj(
        "apiVersion" -> apiVersion,
        "swaggerVersion" -> "1.1",
        "basePath" -> basePath,
        "apis" -> Json.arr(
          Json.obj(
            "path" -> "/api/api-docs.json/api",
            "description" -> "All api"
          )
        )
      )
      Ok(js)

    }

  }



}