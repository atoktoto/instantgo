package controllers

import play.api._
import play.api.mvc._
import play.api.db.DB
import anorm._
import play.api.Play.current
import play.api.libs.json.JsValue
import models.GameRoom
;

object Application extends Controller {

  def socket(playerKey: String) = WebSocket.async[JsValue] { request  =>
    GameRoom.join(playerKey)
  }
  
  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def game(key: String) = Action { implicit request =>
    Ok(views.html.game(key))
  }
  
}