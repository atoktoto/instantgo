package game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GoBoard {

    public static Pattern coordinatePattern = Pattern.compile("([a-s])(1?[0-9])$");
    public static char V_BLACK = '#';
    public static char V_WHITE = '0';
    public static char V_EMPTY = '.';

    int size;
    char[] fields;

    public GoBoard(int size) {
        this.size = size;
        fields = new char[size*size];
        for(int i = 0; i < size*size; i++) {
            fields[i] = V_EMPTY;
        }
    }

    public void fromString(String data) {
        data = data.replaceAll("\\s", "");

        if(data.length() != size*size)
            throw new IllegalArgumentException("string has invalid number of characters ( != size^2 )");

        for(int i = 0; i < data.length(); i++) {
            char c = data.charAt(i);
            if(c != V_BLACK && c != V_WHITE && c != V_EMPTY) {
                throw new IllegalArgumentException("invalid character in string: " + c);
            }
            fields[i] = c;
        }
    }

    public String toString() {
        return new String(fields);
    }

    public char get(Place p) {
        return fields[p.i];
    }

    public void set(Place p, char value) {
        fields[p.i] = value;
    }

    public void set(Iterable<Place> places, char value) {
        for(Place p : places) {
            set(p, value);
        }
    }

    public ArrayList<Place> getAdjacent(Place p, char value) {
        ArrayList<Place> places = new ArrayList<Place>();
        if(p.x > 0 && fields[p.move(-1,0).i] == value){
            places.add(p.move(-1,0));
        }
        if(p.x < size-1 && fields[p.move(1,0).i] == value){
            places.add(p.move(1,0));
        }
        if(p.y > 0 && fields[p.move(0,-1).i] == value){
            places.add(p.move(0,-1));
        }
        if(p.y < size-1 && fields[p.move(0,1).i] == value){
            places.add(p.move(0,1));
        }
        return places;
    }

    public ArrayList<Place> getGroup(Place startPlace) {
        char color = fields[startPlace.i];
        HashSet<Place> added = new HashSet<Place>();
        ArrayList<Place> queue = new ArrayList<Place>();
        ArrayList<Place> result = new ArrayList<Place>();

        queue.add(startPlace);
        while(queue.size() > 0) {
            ArrayList<Place> nextQueue = new ArrayList<Place>();
            for(Place p : queue) {
                if(!added.contains(p)) {
                    result.add(p);
                    added.add(p);
                    nextQueue.addAll(getAdjacent(p, color));
                }
            }
            queue = nextQueue;
        }

        return result;
    }

    public boolean hasLiberties(ArrayList<Place> group) {
        for(Place p : group) {
            if(getAdjacent(p, V_EMPTY).size() > 0)
                return true;
        }
        return false;
    }

    public int playMove(Place place, char color) {
        char enemy_color = (color == V_BLACK ? V_WHITE : V_BLACK);
        ArrayList<Place> enemies;
        int captures = 0;

        if(fields[place.i] != V_EMPTY)
            return -1;

        fields[place.i] = color;

        enemies = getAdjacent(place, enemy_color);
        for(Place e : enemies) {
            if(fields[e.i] != V_EMPTY) {
                ArrayList<Place> group = getGroup(e);
                if(!hasLiberties(group)) {
                    set(group, V_EMPTY);
                    captures += 1;
                }
            }
        }

        if(captures == 0 && !hasLiberties(getGroup(place))) {
            fields[place.i] = V_EMPTY;
            return  -2;
        }

        return captures;
    }

    public void printState() {
        for(int i = 0; i < size*size; i++) {
            if(i % size == 0)
                System.out.println(" ");
            System.out.print(fields[i]);
        }
        System.out.println(" ");
    }



    public class Place {
        public final int i;
        public final int x;
        public final int y;

//        public Place(int i) {
//            this.i = i;
//            x = i % size;
//            y = i / size;
//        }//TODO bad!

        public Place(int x, int y) {
            this.x = x;
            this.y = y;
            i = y * size + x;
        }

        public Place(String cord) {
            Matcher m = coordinatePattern.matcher(cord.toLowerCase());
            if(m.matches() && m.groupCount() == 2) {
                this.x = m.group(1).charAt(0) - 'a';
                this.y = size - Integer.parseInt(m.group(2));
                i = y * size + x;
            } else {
                throw new RuntimeException("bad coordinate format: " + cord);
            }
        }

        public Place move(int x, int y) {
            return new Place(this.x + x,this.y + y);
        }

        public boolean equals(Object object) {
            if (this == object) return true;
            if (!( this.getClass().equals(object.getClass()) )) return false;
            Place otherPlace = (Place) object;
            return this.i == otherPlace.i;
        }

        @Override
        public int hashCode() {
            int result = i;
            result = 31 * result + x;
            result = 31 * result + y;
            result = 31 * result + size;
            return result;
        }
    }

}
