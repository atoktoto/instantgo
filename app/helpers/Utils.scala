package helpers

import java.util.UUID

object Utils {

  val md = java.security.MessageDigest.getInstance("SHA-1")

  def generatePlayerHash:String = {
    val uuid = UUID.randomUUID().toString();
    new sun.misc.BASE64Encoder().encode(md.digest(uuid.getBytes)).replace('/', '-')
  }

}
