package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current
import models.Game._
import anorm.~
import models.Game.MoveResultSuccess
import models.Game.MoveResultFailure
import _root_.game.GoBoard
import controllers.routes
import play.Routes

case class Game(id: Long, board: String, state: String, captured_white: Long, captured_black: Long, result_white: Long, result_black: Long, white_player_key: String, black_player_key: String, spectator_player_key: String) {

  def api_url:String = "/api/game/" + id
  def play_url_spectator:String = routes.Application.game(spectator_player_key).url
  def play_url_white:String = routes.Application.game(white_player_key).url
  def play_url_black:String = routes.Application.game(black_player_key).url

  def makeMove(coordString: String, playerHash: String):MoveResult = {
    val gb:GoBoard = new GoBoard(19);
    gb.fromString(board)

    val place = new gb.Place(coordString)
    val player = checkPlayerHash(playerHash)

    if(player == UnknownPlayer) {
      return new MoveResultFailure("UnknownPlayer")
    }

    if((state != Status.BLACK_PLAY && player == PlayerBlack) || (state != Status.WHITE_PLAY && player == PlayerWhite)) {
      return new MoveResultFailure("NotYourTurn")
    }

    val moveResult = gb.playMove(place, player.color)

    if(moveResult >= 0) {
      val white_captured = if (player == PlayerBlack) 0 else moveResult
      val black_captured = if (player == PlayerBlack) moveResult else 0
      val next_state = if (player == PlayerBlack) Status.WHITE_PLAY else Status.BLACK_PLAY

      val resultingGame = new Game(id, gb.toString, next_state, captured_white + white_captured, captured_black + black_captured, result_white, result_black, white_player_key, black_player_key, spectator_player_key)
      Game.update(resultingGame)

      return new MoveResultSuccess(resultingGame)
    } else {
      return new MoveResultFailure("MoveIllegal")
    }
  }

  def checkPlayerHash(playerHash: String):Player = {
    if(playerHash == black_player_key) {
      PlayerBlack
    } else if(playerHash == white_player_key) {
      PlayerWhite
    } else {
      UnknownPlayer
    }
  }

}

object Game {

  object Status {
    val BLACK_PLAY = "black_play"
    val WHITE_PLAY = "white_play"
    val BLACK_MARK_DEAD = "black_mark_dead"
    val WHITE_MARD_DEAD = "white_mark_dead"
  }

  class MoveResult()
  case class MoveResultSuccess(resultingGameState: Game) extends MoveResult
  case class MoveResultFailure(cause: String) extends MoveResult

  class Player(var color:Char = GoBoard.V_EMPTY)
  object PlayerBlack extends Player(GoBoard.V_BLACK)
  object PlayerWhite extends Player(GoBoard.V_WHITE)
  object UnknownPlayer extends Player

  val game = {
      get[Long]("id") ~
      get[String]("board") ~
      get[String]("state") ~
      get[Long]("captured_white") ~
      get[Long]("captured_black") ~
      get[Long]("result_white") ~
      get[Long]("result_black") ~
      get[String]("white_player_key") ~
      get[String]("black_player_key") ~
      get[String]("spectator_player_key") map {
      case id~board~state~captured_white~captured_black~result_white~result_black~white_player_key~black_player_key~spectator_player_key => Game(id, board, state, captured_white, captured_black, result_white, result_black, white_player_key, black_player_key, spectator_player_key)
    }
  }

  def all(): List[Game] = DB.withConnection { implicit c =>
    SQL("select * from game").as(game *)
  }

  def create(): Game = {
    val id = DB.withConnection { implicit c =>
      SQL("INSERT INTO game values (default)").executeInsert()
    }: Option[Long]
    getById(id.get)
  }

  def update(game:Game) = DB.withConnection { implicit c =>
    SQL("UPDATE game SET board = {board}, state = {state}, captured_white = {captured_white}, captured_black = {captured_black}, result_white = {result_white}, result_black = {result_black}, white_player_key = {white_player_key}, black_player_key = {black_player_key}, spectator_player_key = {spectator_player_key}" +
      "WHERE id = {id}").on('id -> game.id, 'board -> game.board, 'state -> game.state, 'captured_white -> game.captured_white, 'captured_black -> game.captured_black, 'result_white -> game.result_white, 'result_black -> game.result_black, 'white_player_key -> game.white_player_key, 'black_player_key -> game.black_player_key, 'spectator_player_key -> game.spectator_player_key)
    .executeUpdate()
  }

  def getById(id: Long): Game = DB.withConnection { implicit c =>
    SQL("select * from game where id = {id}").on('id -> id).as(game *).head
  }

  def getByPlayerKey(key: String): Game = DB.withConnection { implicit c =>
    SQL("select * from game where white_player_key = {key} or black_player_key = {key} or spectator_player_key = {key}").on('key -> key).as(game *).head
  }

  def delete(id: Long) = DB.withConnection { implicit c =>
    SQL("delete from game where id = {id}").on(
      'id -> id
    ).executeUpdate()
  }

}

