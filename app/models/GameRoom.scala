package models

import akka.actor._
import scala.concurrent.duration._

import play.api._
import play.api.libs.json._
import play.api.libs.iteratee._
import play.api.libs.concurrent._

import akka.util.Timeout
import akka.pattern.ask

import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._
import java.util

object GameRoom {

  implicit val timeout = Timeout(1 second)
  val rooms = new util.HashMap[Long, ActorRef]

  def join(username:String):scala.concurrent.Future[(Iteratee[JsValue,_],Enumerator[JsValue])] = {

    val gameId = Game.getByPlayerKey(username).id

    if(!rooms.containsKey(gameId)) {
      rooms.put(gameId, Akka.system.actorOf(Props[GameRoom]))
    }
    val roomActor = rooms.get(gameId)

    (roomActor ? Join(username)).map {

      case Connected(enumerator) =>

        // Create an Iteratee to consume the feed
        val iteratee = Iteratee.foreach[JsValue] { event =>
          roomActor ! Talk(username, (event \ "text").as[String])
        }.mapDone { _ =>
          roomActor ! Quit(username)
        }

        (iteratee,enumerator)

      case CannotConnect(error) =>

        // Connection error

        // A finished Iteratee sending EOF
        val iteratee = Done[JsValue,Unit]((),Input.EOF)

        // Send an error and close the socket
        val enumerator =  Enumerator[JsValue](JsObject(Seq("error" -> JsString(error)))).andThen(Enumerator.enumInput(Input.EOF))

        (iteratee,enumerator)

    }
  }

  def sendGameActionNotification(gameId: Long, data: JsValue) = {
    if (rooms.containsKey(gameId)) {
      val room = rooms.get(gameId)
      room ! Move(data)
    }
  }

  def closeGameRoom(room: ActorRef) = {
    rooms.remove(room)
    room ! PoisonPill.getInstance
  }

}

class GameRoom extends Actor {

  var members = Set.empty[String]
  val (chatEnumerator, chatChannel) = Concurrent.broadcast[JsValue]

  def receive = {

    case Join(username) => {
      if(members.contains(username)) {
        sender ! CannotConnect("This username is already used")
      } else {
        members = members + username
        sender ! Connected(chatEnumerator)
        self ! NotifyJoin(username)
      }
    }

    case NotifyJoin(username) => {
      notifyAll("join", username, "has entered the room")
    }

    case Talk(username, text) => {
      notifyAll("talk", username, text)
    }

    case Quit(username) => {
      members = members - username
      notifyAll("quit", username, "has left the room")

      if(members.size == 0) {
        GameRoom.closeGameRoom(self)
      }
    }

    case Move(data) => {
      val msg = JsObject(
        Seq(
          "kind" -> JsString("move"),
          "game" -> data
        )
      )
      chatChannel.push(msg)
    }

  }

  def notifyAll(kind: String, user: String, text: String) = {
    val msg = JsObject(
      Seq(
        "kind" -> JsString(kind),
        "user" -> JsString(user),
        "message" -> JsString(text),
        "members" -> JsArray(
          members.toList.map(JsString)
        )
      )
    )
    chatChannel.push(msg)
  }

}

case class Join(username: String)
case class Quit(username: String)
case class Talk(username: String, text: String)
case class NotifyJoin(username: String)
case class Move(data: JsValue)

case class Connected(enumerator:Enumerator[JsValue])
case class CannotConnect(msg: String)