# Games schema

# --- !Ups

CREATE SEQUENCE game_id_seq;
CREATE TABLE game (
    id integer NOT NULL DEFAULT nextval('game_id_seq') PRIMARY KEY,
    board char(361) DEFAULT repeat('.', 361)::char(361),
    state varchar default 'black_play',
    captured_white integer default 0,
    captured_black integer default 0,
    result_white integer default 0,
    result_black integer default 0
);

# --- !Downs

DROP TABLE IF EXISTS game;
DROP SEQUENCE IF EXISTS game_id_seq;