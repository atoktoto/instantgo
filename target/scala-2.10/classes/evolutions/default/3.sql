# --- First database schema

# --- !Ups

alter TABLE game ADD column white_player_key text NOT NULL default md5(random()::text);
alter TABLE game ADD column black_player_key text NOT NULL default md5(random()::text);

# --- !Downs

alter TABLE game DROP column white_player_key;
alter TABLE game DROP column black_player_key;