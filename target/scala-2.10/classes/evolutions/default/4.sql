# --- First database schema

# --- !Ups

alter TABLE game ADD column spectator_player_key text NOT NULL default md5(random()::text);

# --- !Downs

alter TABLE game DROP column spectator_player_key;