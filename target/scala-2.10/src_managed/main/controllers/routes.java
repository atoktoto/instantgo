// @SOURCE:D:/uczelnia/baduk2/conf/routes
// @HASH:fcfaf9f7b68488d75573dc9ea99a6485cbdd3558
// @DATE:Sat May 25 20:52:21 CEST 2013

package controllers;

public class routes {
public static final controllers.ReverseApplication Application = new controllers.ReverseApplication();
public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets();
public static final controllers.ReverseApi Api = new controllers.ReverseApi();
public static final controllers.ReverseApiDocu ApiDocu = new controllers.ReverseApiDocu();
public static class javascript {
public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication();
public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets();
public static final controllers.javascript.ReverseApi Api = new controllers.javascript.ReverseApi();
public static final controllers.javascript.ReverseApiDocu ApiDocu = new controllers.javascript.ReverseApiDocu();    
}   
public static class ref {
public static final controllers.ref.ReverseApplication Application = new controllers.ref.ReverseApplication();
public static final controllers.ref.ReverseAssets Assets = new controllers.ref.ReverseAssets();
public static final controllers.ref.ReverseApi Api = new controllers.ref.ReverseApi();
public static final controllers.ref.ReverseApiDocu ApiDocu = new controllers.ref.ReverseApiDocu();    
} 
}
              