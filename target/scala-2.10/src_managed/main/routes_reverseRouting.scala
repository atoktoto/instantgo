// @SOURCE:D:/uczelnia/baduk2/conf/routes
// @HASH:fcfaf9f7b68488d75573dc9ea99a6485cbdd3558
// @DATE:Sat May 25 20:52:21 CEST 2013

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._


import Router.queryString


// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:19
// @LINE:18
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:11
// @LINE:9
// @LINE:6
package controllers {

// @LINE:19
// @LINE:18
// @LINE:6
class ReverseApplication {
    

// @LINE:19
def socket(key:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "game/socket/" + implicitly[PathBindable[String]].unbind("key", key))
}
                                                

// @LINE:6
def index(): Call = {
   Call("GET", _prefix)
}
                                                

// @LINE:18
def game(key:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "game/" + implicitly[PathBindable[String]].unbind("key", key))
}
                                                
    
}
                          

// @LINE:25
// @LINE:24
// @LINE:9
class ReverseAssets {
    

// @LINE:25
// @LINE:24
// @LINE:9
def at(path:String, file:String): Call = {
   (path: @unchecked, file: @unchecked) match {
// @LINE:9
case (path, file) if path == "/public" => Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
                                                        
// @LINE:24
case (path, file) if path == "/public/swagger" && file == "index.html" => Call("GET", _prefix + { _defaultPrefix } + "swagger/")
                                                        
// @LINE:25
case (path, file) if path == "/public/swagger" => Call("GET", _prefix + { _defaultPrefix } + "swagger/" + implicitly[PathBindable[String]].unbind("file", file))
                                                        
   }
}
                                                
    
}
                          

// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:11
class ReverseApi {
    

// @LINE:13
def createGame(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/game")
}
                                                

// @LINE:15
def getBars(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/bars")
}
                                                

// @LINE:11
def sayHello(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/hello")
}
                                                

// @LINE:16
def playMove(move:String, key:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/game/" + implicitly[PathBindable[String]].unbind("key", key) + "/" + implicitly[PathBindable[String]].unbind("move", move))
}
                                                

// @LINE:14
def getGame(key:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/game/" + implicitly[PathBindable[String]].unbind("key", key))
}
                                                
    
}
                          

// @LINE:23
// @LINE:22
class ReverseApiDocu {
    

// @LINE:23
// @LINE:22
def index(api:String): Call = {
   (api: @unchecked) match {
// @LINE:22
case (api) if true => Call("GET", _prefix + { _defaultPrefix } + "api/api-docs.json/" + implicitly[PathBindable[String]].unbind("api", api))
                                                        
// @LINE:23
case (api) if api == "" => Call("GET", _prefix + { _defaultPrefix } + "api/api-docs.json")
                                                        
   }
}
                                                
    
}
                          
}
                  


// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:19
// @LINE:18
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:11
// @LINE:9
// @LINE:6
package controllers.javascript {

// @LINE:19
// @LINE:18
// @LINE:6
class ReverseApplication {
    

// @LINE:19
def socket : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.socket",
   """
      function(key) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "game/socket/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("key", key)})
      }
   """
)
                        

// @LINE:6
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        

// @LINE:18
def game : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.game",
   """
      function(key) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "game/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("key", key)})
      }
   """
)
                        
    
}
              

// @LINE:25
// @LINE:24
// @LINE:9
class ReverseAssets {
    

// @LINE:25
// @LINE:24
// @LINE:9
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(path, file) {
      if (path == """ + implicitly[JavascriptLitteral[String]].to("/public") + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
      if (path == """ + implicitly[JavascriptLitteral[String]].to("/public/swagger") + """ && file == """ + implicitly[JavascriptLitteral[String]].to("index.html") + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "swagger/"})
      }
      if (path == """ + implicitly[JavascriptLitteral[String]].to("/public/swagger") + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "swagger/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
      }
   """
)
                        
    
}
              

// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:11
class ReverseApi {
    

// @LINE:13
def createGame : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Api.createGame",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/game"})
      }
   """
)
                        

// @LINE:15
def getBars : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Api.getBars",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/bars"})
      }
   """
)
                        

// @LINE:11
def sayHello : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Api.sayHello",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/hello"})
      }
   """
)
                        

// @LINE:16
def playMove : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Api.playMove",
   """
      function(move,key) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/game/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("key", key) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("move", move)})
      }
   """
)
                        

// @LINE:14
def getGame : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Api.getGame",
   """
      function(key) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/game/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("key", key)})
      }
   """
)
                        
    
}
              

// @LINE:23
// @LINE:22
class ReverseApiDocu {
    

// @LINE:23
// @LINE:22
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ApiDocu.index",
   """
      function(api) {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/api-docs.json/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("api", api)})
      }
      if (api == """ + implicitly[JavascriptLitteral[String]].to("") + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/api-docs.json"})
      }
      }
   """
)
                        
    
}
              
}
        


// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:19
// @LINE:18
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:11
// @LINE:9
// @LINE:6
package controllers.ref {

// @LINE:19
// @LINE:18
// @LINE:6
class ReverseApplication {
    

// @LINE:19
def socket(key:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.socket(key), HandlerDef(this, "controllers.Application", "socket", Seq(classOf[String]), "GET", """""", _prefix + """game/socket/$key<[^/]+>""")
)
                      

// @LINE:6
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Seq(), "GET", """ Home page""", _prefix + """""")
)
                      

// @LINE:18
def game(key:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.game(key), HandlerDef(this, "controllers.Application", "game", Seq(classOf[String]), "GET", """""", _prefix + """game/$key<[^/]+>""")
)
                      
    
}
                          

// @LINE:25
// @LINE:24
// @LINE:9
class ReverseAssets {
    

// @LINE:9
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      
    
}
                          

// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:11
class ReverseApi {
    

// @LINE:13
def createGame(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Api.createGame(), HandlerDef(this, "controllers.Api", "createGame", Seq(), "POST", """""", _prefix + """api/game""")
)
                      

// @LINE:15
def getBars(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Api.getBars(), HandlerDef(this, "controllers.Api", "getBars", Seq(), "GET", """""", _prefix + """api/bars""")
)
                      

// @LINE:11
def sayHello(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Api.sayHello(), HandlerDef(this, "controllers.Api", "sayHello", Seq(), "POST", """""", _prefix + """api/hello""")
)
                      

// @LINE:16
def playMove(move:String, key:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Api.playMove(move, key), HandlerDef(this, "controllers.Api", "playMove", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """api/game/$key<[^/]+>/$move<[^/]+>""")
)
                      

// @LINE:14
def getGame(key:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Api.getGame(key), HandlerDef(this, "controllers.Api", "getGame", Seq(classOf[String]), "GET", """""", _prefix + """api/game/$key<[^/]+>""")
)
                      
    
}
                          

// @LINE:23
// @LINE:22
class ReverseApiDocu {
    

// @LINE:22
def index(api:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.ApiDocu.index(api), HandlerDef(this, "controllers.ApiDocu", "index", Seq(classOf[String]), "GET", """ swagger""", _prefix + """api/api-docs.json/$api<[^/]+>""")
)
                      
    
}
                          
}
                  
      