// @SOURCE:D:/uczelnia/baduk2/conf/routes
// @HASH:fcfaf9f7b68488d75573dc9ea99a6485cbdd3558
// @DATE:Sat May 25 20:52:21 CEST 2013


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._


import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix  
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" } 


// @LINE:6
private[this] lazy val controllers_Application_index0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
        

// @LINE:9
private[this] lazy val controllers_Assets_at1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+"""))))
        

// @LINE:11
private[this] lazy val controllers_Api_sayHello2 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/hello"))))
        

// @LINE:13
private[this] lazy val controllers_Api_createGame3 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/game"))))
        

// @LINE:14
private[this] lazy val controllers_Api_getGame4 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/game/"),DynamicPart("key", """[^/]+"""))))
        

// @LINE:15
private[this] lazy val controllers_Api_getBars5 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/bars"))))
        

// @LINE:16
private[this] lazy val controllers_Api_playMove6 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/game/"),DynamicPart("key", """[^/]+"""),StaticPart("/"),DynamicPart("move", """[^/]+"""))))
        

// @LINE:18
private[this] lazy val controllers_Application_game7 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("game/"),DynamicPart("key", """[^/]+"""))))
        

// @LINE:19
private[this] lazy val controllers_Application_socket8 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("game/socket/"),DynamicPart("key", """[^/]+"""))))
        

// @LINE:22
private[this] lazy val controllers_ApiDocu_index9 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/api-docs.json/"),DynamicPart("api", """[^/]+"""))))
        

// @LINE:23
private[this] lazy val controllers_ApiDocu_index10 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/api-docs.json"))))
        

// @LINE:24
private[this] lazy val controllers_Assets_at11 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("swagger/"))))
        

// @LINE:25
private[this] lazy val controllers_Assets_at12 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("swagger/"),DynamicPart("file", """.+"""))))
        
def documentation = List(("""GET""", prefix,"""controllers.Application.index"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/hello""","""controllers.Api.sayHello"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/game""","""controllers.Api.createGame()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/game/$key<[^/]+>""","""controllers.Api.getGame(key:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/bars""","""controllers.Api.getBars()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/game/$key<[^/]+>/$move<[^/]+>""","""controllers.Api.playMove(move:String, key:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """game/$key<[^/]+>""","""controllers.Application.game(key:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """game/socket/$key<[^/]+>""","""controllers.Application.socket(key:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/api-docs.json/$api<[^/]+>""","""controllers.ApiDocu.index(api:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/api-docs.json""","""controllers.ApiDocu.index(api:String = "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """swagger/""","""controllers.Assets.at(path:String = "/public/swagger", file:String = "index.html")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """swagger/$file<.+>""","""controllers.Assets.at(path:String = "/public/swagger", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
       
    
def routes:PartialFunction[RequestHeader,Handler] = {        

// @LINE:6
case controllers_Application_index0(params) => {
   call { 
        invokeHandler(controllers.Application.index, HandlerDef(this, "controllers.Application", "index", Nil,"GET", """ Home page""", Routes.prefix + """"""))
   }
}
        

// @LINE:9
case controllers_Assets_at1(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        

// @LINE:11
case controllers_Api_sayHello2(params) => {
   call { 
        invokeHandler(controllers.Api.sayHello, HandlerDef(this, "controllers.Api", "sayHello", Nil,"POST", """""", Routes.prefix + """api/hello"""))
   }
}
        

// @LINE:13
case controllers_Api_createGame3(params) => {
   call { 
        invokeHandler(controllers.Api.createGame(), HandlerDef(this, "controllers.Api", "createGame", Nil,"POST", """""", Routes.prefix + """api/game"""))
   }
}
        

// @LINE:14
case controllers_Api_getGame4(params) => {
   call(params.fromPath[String]("key", None)) { (key) =>
        invokeHandler(controllers.Api.getGame(key), HandlerDef(this, "controllers.Api", "getGame", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/game/$key<[^/]+>"""))
   }
}
        

// @LINE:15
case controllers_Api_getBars5(params) => {
   call { 
        invokeHandler(controllers.Api.getBars(), HandlerDef(this, "controllers.Api", "getBars", Nil,"GET", """""", Routes.prefix + """api/bars"""))
   }
}
        

// @LINE:16
case controllers_Api_playMove6(params) => {
   call(params.fromPath[String]("move", None), params.fromPath[String]("key", None)) { (move, key) =>
        invokeHandler(controllers.Api.playMove(move, key), HandlerDef(this, "controllers.Api", "playMove", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """api/game/$key<[^/]+>/$move<[^/]+>"""))
   }
}
        

// @LINE:18
case controllers_Application_game7(params) => {
   call(params.fromPath[String]("key", None)) { (key) =>
        invokeHandler(controllers.Application.game(key), HandlerDef(this, "controllers.Application", "game", Seq(classOf[String]),"GET", """""", Routes.prefix + """game/$key<[^/]+>"""))
   }
}
        

// @LINE:19
case controllers_Application_socket8(params) => {
   call(params.fromPath[String]("key", None)) { (key) =>
        invokeHandler(controllers.Application.socket(key), HandlerDef(this, "controllers.Application", "socket", Seq(classOf[String]),"GET", """""", Routes.prefix + """game/socket/$key<[^/]+>"""))
   }
}
        

// @LINE:22
case controllers_ApiDocu_index9(params) => {
   call(params.fromPath[String]("api", None)) { (api) =>
        invokeHandler(controllers.ApiDocu.index(api), HandlerDef(this, "controllers.ApiDocu", "index", Seq(classOf[String]),"GET", """ swagger""", Routes.prefix + """api/api-docs.json/$api<[^/]+>"""))
   }
}
        

// @LINE:23
case controllers_ApiDocu_index10(params) => {
   call(Param[String]("api", Right(""))) { (api) =>
        invokeHandler(controllers.ApiDocu.index(api), HandlerDef(this, "controllers.ApiDocu", "index", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/api-docs.json"""))
   }
}
        

// @LINE:24
case controllers_Assets_at11(params) => {
   call(Param[String]("path", Right("/public/swagger")), Param[String]("file", Right("index.html"))) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """swagger/"""))
   }
}
        

// @LINE:25
case controllers_Assets_at12(params) => {
   call(Param[String]("path", Right("/public/swagger")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """swagger/$file<.+>"""))
   }
}
        
}
    
}
        