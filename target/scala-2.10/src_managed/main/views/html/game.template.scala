
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
/**/
object game extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,RequestHeader,play.api.templates.Html] {

    /**/
    def apply/*1.2*/(key: String)(implicit request: RequestHeader):play.api.templates.Html = {
        _display_ {

Seq[Any](format.raw/*1.48*/("""

"""),_display_(Seq[Any](/*3.2*/main("Instant game of GO")/*3.28*/ {_display_(Seq[Any](format.raw/*3.30*/("""

    <div class="row">
        <div class="span9"><div class="jgo_board" id="board"></div></div>
        <div class="span3">
            <div>
                You play <b id="color_text">...</b>.
                Turn: <b id="turn_text">...</b>. <br/><br/>
                Captured: <br/>
                black <b id="black_captured_text">0</b> /
                white <b id="white_captured_text">0</b><br/>
            </div>
            <h3>Messages</h3>
            <div id="messages"></div>
        </div>
    </div>


    <script type="text/javascript" charset="utf-8">

        var key = """"),_display_(Seq[Any](/*23.21*/key)),format.raw/*23.24*/("""";
        var ownColor = "";
        var gameData;
        var myTurn;

        $(document).ready(function()"""),format.raw/*28.37*/("""{"""),format.raw/*28.38*/(""" // jQuery way
            board = jgo_generateBoard($("#board"));
            board.click = boardClick

            $.get("/api/game/" + key, function(game) """),format.raw/*32.54*/("""{"""),format.raw/*32.55*/("""
                showGameState(game)
                $("#color_text").text(ownColor)
            """),format.raw/*35.13*/("""}"""),format.raw/*35.14*/(""");
        """),format.raw/*36.9*/("""}"""),format.raw/*36.10*/(""");

        function keyToColor(k) """),format.raw/*38.32*/("""{"""),format.raw/*38.33*/("""
            if(gameData.black_player_key == k) """),format.raw/*39.48*/("""{"""),format.raw/*39.49*/("""
                return "black"
            """),format.raw/*41.13*/("""}"""),format.raw/*41.14*/(""" else if(gameData.white_player_key == k) """),format.raw/*41.55*/("""{"""),format.raw/*41.56*/("""
                return "white"
            """),format.raw/*43.13*/("""}"""),format.raw/*43.14*/("""
        """),format.raw/*44.9*/("""}"""),format.raw/*44.10*/("""

        function boardClick(coord) """),format.raw/*46.36*/("""{"""),format.raw/*46.37*/("""
            var stone = board.get(coord);
            if(stone == JGO_CLEAR && myTurn == true) """),format.raw/*48.54*/("""{"""),format.raw/*48.55*/("""
                $.ajax("""),format.raw/*49.24*/("""{"""),format.raw/*49.25*/("""
                    url: "/api/game/"""),_display_(Seq[Any](/*50.38*/key)),format.raw/*50.41*/("""/" + coord,
                    success: function(data) """),format.raw/*51.45*/("""{"""),format.raw/*51.46*/("""
                        showGameState(data)
                    """),format.raw/*53.21*/("""}"""),format.raw/*53.22*/("""
                """),format.raw/*54.17*/("""}"""),format.raw/*54.18*/(""").fail(function(data) """),format.raw/*54.40*/("""{"""),format.raw/*54.41*/("""
                    alert("error: " + JSON.stringify(data));
                    addErrorMessage(data)
                """),format.raw/*57.17*/("""}"""),format.raw/*57.18*/(""");
            """),format.raw/*58.13*/("""}"""),format.raw/*58.14*/("""
        """),format.raw/*59.9*/("""}"""),format.raw/*59.10*/("""

        function addErrorMessage(data) """),format.raw/*61.40*/("""{"""),format.raw/*61.41*/("""
            var el = $('<div class="message"><span></span><p></p></div>')
            $("p", el).text(data.responseText)
            $(el).addClass("error_message")
            $('#messages').append(el)
        """),format.raw/*66.9*/("""}"""),format.raw/*66.10*/("""

        function addChatMessage(data) """),format.raw/*68.39*/("""{"""),format.raw/*68.40*/("""
            var el = $('<div class="message"><b></b><p></p></div>')
            $("b", el).text(keyToColor(data.user))
            $("p", el).text(data.message)
            $(el).addClass(data.kind)
            $('#messages').append(el)
        """),format.raw/*74.9*/("""}"""),format.raw/*74.10*/("""

        function showGameState(game) """),format.raw/*76.38*/("""{"""),format.raw/*76.39*/("""
            gameData = game;
            ownColor = keyToColor(key)
            board.fromString(game.board)
            if(game.state == "black_play") """),format.raw/*80.44*/("""{"""),format.raw/*80.45*/("""
                $("#turn_text").text("black");
                myTurn = ownColor == "black";
            """),format.raw/*83.13*/("""}"""),format.raw/*83.14*/(""" else if(game.state == "white_play") """),format.raw/*83.51*/("""{"""),format.raw/*83.52*/("""
                $("#turn_text").text("white");
                myTurn = ownColor == "white";
            """),format.raw/*86.13*/("""}"""),format.raw/*86.14*/("""

            $("#black_captured_text").text(game.captured_black)
            $("#white_captured_text").text(game.captured_white)
        """),format.raw/*90.9*/("""}"""),format.raw/*90.10*/("""

        $(function() """),format.raw/*92.22*/("""{"""),format.raw/*92.23*/("""

            var WS = window['MozWebSocket'] ? MozWebSocket : WebSocket
            var chatSocket = new WS("ws://go-atok.rhcloud.com:8000/game/socket/"""),_display_(Seq[Any](/*95.81*/key)),format.raw/*95.84*/("""");

            var sendMessage = function() """),format.raw/*97.42*/("""{"""),format.raw/*97.43*/("""
                chatSocket.send(JSON.stringify(
                    """),format.raw/*99.21*/("""{"""),format.raw/*99.22*/("""text: $("#talk").val()"""),format.raw/*99.44*/("""}"""),format.raw/*99.45*/("""
                ))
                $("#talk").val('')
            """),format.raw/*102.13*/("""}"""),format.raw/*102.14*/("""

            var receiveEvent = function(event) """),format.raw/*104.48*/("""{"""),format.raw/*104.49*/("""
                var data = JSON.parse(event.data)

                // Handle errors
                if(data.error) """),format.raw/*108.32*/("""{"""),format.raw/*108.33*/("""
                    chatSocket.close()
                    $("#onError span").text(data.error)
                    $("#onError").show()
                    return
                """),format.raw/*113.17*/("""}"""),format.raw/*113.18*/(""" else """),format.raw/*113.24*/("""{"""),format.raw/*113.25*/("""
                    $("#onChat").show()
                """),format.raw/*115.17*/("""}"""),format.raw/*115.18*/("""

                if(data.kind == "move") """),format.raw/*117.41*/("""{"""),format.raw/*117.42*/("""
                    showGameState(data.game)
                """),format.raw/*119.17*/("""}"""),format.raw/*119.18*/(""" else """),format.raw/*119.24*/("""{"""),format.raw/*119.25*/("""
                    addChatMessage(data)
                """),format.raw/*121.17*/("""}"""),format.raw/*121.18*/("""
            """),format.raw/*122.13*/("""}"""),format.raw/*122.14*/("""

            var handleReturnKey = function(e) """),format.raw/*124.47*/("""{"""),format.raw/*124.48*/("""
                if(e.charCode == 13 || e.keyCode == 13) """),format.raw/*125.57*/("""{"""),format.raw/*125.58*/("""
                e.preventDefault()
                sendMessage()
                """),format.raw/*128.17*/("""}"""),format.raw/*128.18*/("""
            """),format.raw/*129.13*/("""}"""),format.raw/*129.14*/("""

            $("#talk").keypress(handleReturnKey)
            chatSocket.onmessage = receiveEvent
        """),format.raw/*133.9*/("""}"""),format.raw/*133.10*/(""")

    </script>
""")))})),format.raw/*136.2*/("""
"""))}
    }
    
    def render(key:String,request:RequestHeader): play.api.templates.Html = apply(key)(request)
    
    def f:((String) => (RequestHeader) => play.api.templates.Html) = (key) => (request) => apply(key)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Sat May 25 20:52:22 CEST 2013
                    SOURCE: D:/uczelnia/baduk2/app/views/game.scala.html
                    HASH: 7901a1a6083620eb7ac4a79b6b9674cb1d325002
                    MATRIX: 518->1|641->47|680->52|714->78|753->80|1405->696|1430->699|1572->813|1601->814|1791->976|1820->977|1948->1077|1977->1078|2016->1090|2045->1091|2110->1128|2139->1129|2216->1178|2245->1179|2319->1225|2348->1226|2417->1267|2446->1268|2520->1314|2549->1315|2586->1325|2615->1326|2682->1365|2711->1366|2837->1464|2866->1465|2919->1490|2948->1491|3023->1530|3048->1533|3133->1590|3162->1591|3257->1658|3286->1659|3332->1677|3361->1678|3411->1700|3440->1701|3591->1824|3620->1825|3664->1841|3693->1842|3730->1852|3759->1853|3830->1896|3859->1897|4103->2114|4132->2115|4202->2157|4231->2158|4510->2410|4539->2411|4608->2452|4637->2453|4822->2610|4851->2611|4988->2720|5017->2721|5082->2758|5111->2759|5248->2868|5277->2869|5446->3011|5475->3012|5528->3037|5557->3038|5749->3194|5774->3197|5850->3245|5879->3246|5978->3317|6007->3318|6057->3340|6086->3341|6185->3411|6215->3412|6295->3463|6325->3464|6474->3584|6504->3585|6718->3770|6748->3771|6783->3777|6813->3778|6901->3837|6931->3838|7004->3882|7034->3883|7127->3947|7157->3948|7192->3954|7222->3955|7311->4015|7341->4016|7384->4030|7414->4031|7493->4081|7523->4082|7610->4140|7640->4141|7754->4226|7784->4227|7827->4241|7857->4242|7996->4353|8026->4354|8079->4375
                    LINES: 19->1|22->1|24->3|24->3|24->3|44->23|44->23|49->28|49->28|53->32|53->32|56->35|56->35|57->36|57->36|59->38|59->38|60->39|60->39|62->41|62->41|62->41|62->41|64->43|64->43|65->44|65->44|67->46|67->46|69->48|69->48|70->49|70->49|71->50|71->50|72->51|72->51|74->53|74->53|75->54|75->54|75->54|75->54|78->57|78->57|79->58|79->58|80->59|80->59|82->61|82->61|87->66|87->66|89->68|89->68|95->74|95->74|97->76|97->76|101->80|101->80|104->83|104->83|104->83|104->83|107->86|107->86|111->90|111->90|113->92|113->92|116->95|116->95|118->97|118->97|120->99|120->99|120->99|120->99|123->102|123->102|125->104|125->104|129->108|129->108|134->113|134->113|134->113|134->113|136->115|136->115|138->117|138->117|140->119|140->119|140->119|140->119|142->121|142->121|143->122|143->122|145->124|145->124|146->125|146->125|149->128|149->128|150->129|150->129|154->133|154->133|157->136
                    -- GENERATED --
                */
            