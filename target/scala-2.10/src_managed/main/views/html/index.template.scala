
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
/**/
object index extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template1[String,play.api.templates.Html] {

    /**/
    def apply/*1.2*/(message: String):play.api.templates.Html = {
        _display_ {

Seq[Any](format.raw/*1.19*/("""

"""),_display_(Seq[Any](/*3.2*/main("Instant game of GO")/*3.28*/ {_display_(Seq[Any](format.raw/*3.30*/("""

    <button id="new_game">Create new game</button>
    <script>

        $(function() """),format.raw/*8.22*/("""{"""),format.raw/*8.23*/("""
            $('#game_links').hide()
        """),format.raw/*10.9*/("""}"""),format.raw/*10.10*/(""")

        $("#new_game").click(function() """),format.raw/*12.41*/("""{"""),format.raw/*12.42*/("""
            var url = "/api/game";
            $.ajax("""),format.raw/*14.20*/("""{"""),format.raw/*14.21*/("""
                type: "POST",
                url: url,
                success: function(data) """),format.raw/*17.41*/("""{"""),format.raw/*17.42*/("""
                    $('#black_play_link').attr('href', data.play_url_black)
                    $('#white_play_link').attr('href', data.play_url_white)
                    $('#game_links').show()
                """),format.raw/*21.17*/("""}"""),format.raw/*21.18*/("""
            """),format.raw/*22.13*/("""}"""),format.raw/*22.14*/(""")
            return false;
        """),format.raw/*24.9*/("""}"""),format.raw/*24.10*/(""");
    </script>

    <div id="game_links">
        <h2>Game links</h2>
        <a id="black_play_link">Black</a>
        <a id="white_play_link">White</a>
    </div>

""")))})),format.raw/*33.2*/("""
"""))}
    }
    
    def render(message:String): play.api.templates.Html = apply(message)
    
    def f:((String) => play.api.templates.Html) = (message) => apply(message)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Sat May 25 20:52:22 CEST 2013
                    SOURCE: D:/uczelnia/baduk2/app/views/index.scala.html
                    HASH: 775025107127b02edf73218ac23d736963f10d2d
                    MATRIX: 505->1|599->18|638->23|672->49|711->51|831->144|859->145|933->192|962->193|1035->238|1064->239|1149->296|1178->297|1306->397|1335->398|1580->615|1609->616|1651->630|1680->631|1745->669|1774->670|1983->848
                    LINES: 19->1|22->1|24->3|24->3|24->3|29->8|29->8|31->10|31->10|33->12|33->12|35->14|35->14|38->17|38->17|42->21|42->21|43->22|43->22|45->24|45->24|54->33
                    -- GENERATED --
                */
            