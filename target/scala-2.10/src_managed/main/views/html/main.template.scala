
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
/**/
object main extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,Html,play.api.templates.Html] {

    /**/
    def apply/*1.2*/(title: String)(content: Html):play.api.templates.Html = {
        _display_ {

Seq[Any](format.raw/*1.32*/("""

<!DOCTYPE html>

<html>
    <head>
        <title>"""),_display_(Seq[Any](/*7.17*/title)),format.raw/*7.22*/("""</title>
        <meta charset="utf-8">
        <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*9.54*/routes/*9.60*/.Assets.at("/public", "css/bootstrap.min.css"))),format.raw/*9.106*/("""">
        <style>
            body """),format.raw/*11.18*/("""{"""),format.raw/*11.19*/("""
            padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            """),format.raw/*13.13*/("""}"""),format.raw/*13.14*/("""
        </style>
        <link rel="shortcut icon" type="image/png" href=""""),_display_(Seq[Any](/*15.59*/routes/*15.65*/.Assets.at("/public", "img/favicon.png"))),format.raw/*15.105*/("""">
        <script src=""""),_display_(Seq[Any](/*16.23*/routes/*16.29*/.Assets.at("/public", "js/jquery-1.9.0.min.js"))),format.raw/*16.76*/("""" type="text/javascript"></script>
        <script src=""""),_display_(Seq[Any](/*17.23*/routes/*17.29*/.Assets.at("/public", "jgo/jgoboard.js"))),format.raw/*17.69*/("""" type="text/javascript"></script>
        <link rel="stylesheet" href=""""),_display_(Seq[Any](/*18.39*/routes/*18.45*/.Assets.at("/public", "jgo/jgoboard.css"))),format.raw/*18.86*/("""" type="text/css" />
    </head>
    <body>

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="brand" href="#">Instant game of GO</a>
                    <div class="nav-collapse collapse"><!--
                        <ul class="nav">
                            <li class="active"><a href="#">New game</a></li>
                            <li><a href="#about">About</a></li>
                            <li><a href="#contact">Contact</a></li>
                        </ul> -->
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>

        <div class="container">

            """),_display_(Seq[Any](/*44.14*/content)),format.raw/*44.21*/("""

        </div> <!-- /container -->


    </body>
</html>
"""))}
    }
    
    def render(title:String,content:Html): play.api.templates.Html = apply(title)(content)
    
    def f:((String) => (Html) => play.api.templates.Html) = (title) => (content) => apply(title)(content)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Sat May 25 20:52:22 CEST 2013
                    SOURCE: D:/uczelnia/baduk2/app/views/main.scala.html
                    HASH: fa86d2c649714caf46fcd893589aa35684aa208f
                    MATRIX: 509->1|616->31|704->84|730->89|858->182|872->188|940->234|1004->270|1033->271|1181->391|1210->392|1322->468|1337->474|1400->514|1461->539|1476->545|1545->592|1638->649|1653->655|1715->695|1824->768|1839->774|1902->815|3001->1878|3030->1885
                    LINES: 19->1|22->1|28->7|28->7|30->9|30->9|30->9|32->11|32->11|34->13|34->13|36->15|36->15|36->15|37->16|37->16|37->16|38->17|38->17|38->17|39->18|39->18|39->18|65->44|65->44
                    -- GENERATED --
                */
            