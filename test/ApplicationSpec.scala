package test

import org.specs2.mutable._

import play.api.test._
import play.api.test.Helpers._
import game.GoBoard
import helpers.Utils

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class ApplicationSpec extends Specification {

  GoBoard.V_BLACK

  "Utils" should {
    "work" in {
      print("Len: " + Utils.generatePlayerHash.length + " ")
      print(Utils.generatePlayerHash)
    }
  }
  
  "Board" should {

    "read board state from string" in {
      val board: GoBoard = new GoBoard(5)
      board.fromString(
          ".#..." +
          "#.#.." +
          ".#..." +
          "....." +
          ".....")
      board.get(new board.Place(0,0)) must_== GoBoard.V_EMPTY
      board.get(new board.Place(1,0)) must_== GoBoard.V_BLACK
    }

    "calculate groups" in {
      val board: GoBoard = new GoBoard(5)
      board.fromString(
        ".#..." +
        "####." +
        ".#..." +
        "...#." +
        "..##.")

      board.getGroup(new board.Place(1,1)).size() must_== 6
      board.getGroup(new board.Place(3,3)).size() must_== 3
    }

    "check liberties" in {
      val board: GoBoard = new GoBoard(5)
      board.fromString(
          ".#..." +
          "#0#.." +
          "#00.." +
          "###.." +
          "00#..")

      board.hasLiberties(board.getGroup(new board.Place(0,4))) must_== false
      board.hasLiberties(board.getGroup(new board.Place(1,1))) must_== true
    }
    
    "accept legal moves" in {
      val board: GoBoard = new GoBoard(19)
      val moveResult = board.playMove(new board.Place(4,4), GoBoard.V_BLACK)
      moveResult must_== 0
    }

    "persist legal moves" in {
      val board: GoBoard = new GoBoard(19)
      val moveResult = board.playMove((new board.Place(5,5)), GoBoard.V_BLACK)
      board.get(new board.Place(5,5)) must_== GoBoard.V_BLACK
    }

    "prevent suicidal movements" in {
      val board: GoBoard = new GoBoard(5)
      board.fromString( ".#..." +
                        "#.#.." +
                        ".#..." +
                        "....." +
                        ".....")
      val moveResult = board.playMove(new board.Place(1,1), GoBoard.V_WHITE)
      moveResult must_== -2
    }

    "accept winning movements" in {
      val board: GoBoard = new GoBoard(5)
      board.fromString(
        ".#0.." +
        "#.#0." +
        ".#0.." +
        "....." +
        ".....")
      val moveResult = board.playMove(new board.Place(1,1), GoBoard.V_WHITE)
      moveResult must_== 1
    }

    "removes dead stones" in {
      val board: GoBoard = new GoBoard(5)
      board.fromString(
          "....." +
          "...0." +
          "..0#0" +
          "....." +
          ".....")
      val moveResult = board.playMove(new board.Place(3,3), GoBoard.V_WHITE)
      moveResult must_== 1
      board.get(new board.Place(3,2)) must_== GoBoard.V_EMPTY
    }

  }
}